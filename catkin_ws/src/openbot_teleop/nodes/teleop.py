#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Range
import select
import tty
import termios
import threading
import argparse
import sys


LIN_VEL_STEP_SIZE = 0.1
ANG_VEL_STEP_SIZE = 0.1

msg = """
Moving around:
        w
   a    s    d
        x

space key, s : force stop
"""

collision_possibility = False
collision_queue = []
collision_dist = 0.1

#get key function from stdin without waiting on newline
def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key


def callback(data):
    global collision_queue
    global collision_possibility
    global collision_dist

    collision_queue.append(data.range)
    if len(collision_queue) >= 10:
        collision_queue.pop(0)
    mean_value = sum(collision_queue)/len(collision_queue)

    if mean_value < collision_dist and collision_possibility is False:
        collision_possibility = True
    if mean_value > collision_dist:
        collision_possibility = False


def subscriber_handler():
    rospy.Subscriber("ultrasound", Range, callback)
    rospy.spin()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Teleop openbot.')
    parser.add_argument('--collision', action='store_true', default=False, help='If is set the range sensor will be used to prevent collision')
    parser.add_argument('--collision-dist', action='store', type=float, default=0.1, help='Minimal distance that is interpreted as collision')
    args = parser.parse_args()

    collision_dist = args.collision_dist
    settings = termios.tcgetattr(sys.stdin)

    rospy.init_node('openbot_teleop')
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

    counter = 0
    target_linear_vel = 0.0
    target_angular_vel = 0.0
    control_linear_vel = 0.0
    control_angular_vel = 0.0

    x = threading.Thread(target=subscriber_handler)
    if args.collision:
        x.start()

    try:
        print(msg)
        while(1):
            key = getKey()
            if key == 'w':
                target_linear_vel = target_linear_vel + LIN_VEL_STEP_SIZE
                counter = counter + 1
            elif key == 'x':
                target_linear_vel = target_linear_vel - LIN_VEL_STEP_SIZE
                counter = counter + 1
            elif key == 'a':
                target_angular_vel = target_angular_vel + ANG_VEL_STEP_SIZE
                counter = counter + 1
            elif key == 'd':
                target_angular_vel = target_angular_vel - ANG_VEL_STEP_SIZE
                counter = counter + 1
            elif key == ' ' or key == 's':
                target_linear_vel = 0.0
                control_linear_vel = 0.0
                target_angular_vel = 0.0
                control_angular_vel = 0.0
            else:
                if (key == '\x03'):
                    break

            print("currently:\tlinear vel %s\t angular vel %s " % (target_linear_vel, target_angular_vel))
            if counter == 20:
                print(msg)
                counter = 0

            twist = Twist()

            control_linear_vel = target_linear_vel
            twist.linear.x = 0.0 if args.collision and collision_possibility and control_linear_vel > 0 else control_linear_vel
            twist.linear.y = 0.0
            twist.linear.z = 0.0

            control_angular_vel = target_angular_vel
            twist.angular.x = 0.0
            twist.angular.y = 0.0
            twist.angular.z = control_angular_vel

            pub.publish(twist)

    except Exception as ex:
        print(ex)

    finally:
        twist = Twist()
        twist.linear.x = 0.0
        twist.linear.y = 0.0
        twist.linear.z = 0.0
        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = 0.0
        pub.publish(twist)

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)

    if args.collision:
        x.join()
